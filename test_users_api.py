import requests
import pytest

API_URL = "https://assessment-qa.dsocx.com/api"

@pytest.fixture
def user_data():
    return {
        "firstName": "John",
        "lastName": "Doe",
        "email": "johndoe@example.com"
    }

def test_retrieve_user_list():
    response = requests.get(f"{API_URL}/users")
    assert response.status_code == 200
    user_list = response.json()
    assert isinstance(user_list, list)

def test_retrieve_user_details(user_data):
    response = requests.post(f"{API_URL}/users", json=user_data)
    created_user = response.json()

    retrieved_response = requests.get(f"{API_URL}/users/{created_user['id']}")
    assert retrieved_response.status_code == 200
    retrieved_user = retrieved_response.json()
    assert retrieved_user == created_user

def test_create_user(user_data):
    response = requests.post(f"{API_URL}/users", json=user_data)
    assert response.status_code == 201
    created_user = response.json()
    assert created_user["firstName"] == user_data["firstName"]
    assert created_user["lastName"] == user_data["lastName"]
    assert created_user["email"] == user_data["email"]
    assert "id" in created_user
    assert "created" in created_user
    assert "isactive" in created_user
    assert "lastLogin" in created_user

def test_update_user(user_data):
    response = requests.post(f"{API_URL}/users", json=user_data)
    created_user = response.json()

    updated_data = {
        "firstName": "Updated John",
        "lastName": "Updated Doe"
    }
    
    response = requests.put(f"{API_URL}/users/{created_user['id']}", json=updated_data)
    assert response.status_code == 201

def test_invalid_user_creation():
    invalid_user_data = {
        "firstName": "Invalid User"
    }
    response = requests.post(f"{API_URL}/users", json=invalid_user_data)
    assert response.status_code == 400

def test_reset_api():
    response = requests.post(f"{API_URL}/users/reset")
    assert response.status_code == 200

def test_update_nonexistent_user():
    updated_data = {
        "firstName": "Updated John",
        "lastName": "Updated Doe"
    }
    response = requests.put(f"{API_URL}/users/nonexistent_user_id", json=updated_data)
    assert response.status_code == 404

def test_empty_user_list():
    # Ensure the user list is empty after resetting the API
    reset_response = requests.post(f"{API_URL}/users/reset")
    assert reset_response.status_code == 200

    response = requests.get(f"{API_URL}/users")
    user_list = response.json()
    assert response.status_code == 200
    assert isinstance(user_list, list)
    assert len(user_list) == 0
